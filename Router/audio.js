const express = require('express')
const router = express.Router()
const audio = require('../Schema/audio')
const constant = require('../Constant')
const path = require('path')
const { generateuploadURLToS3 } = require('../Functions/S3')

router.get("/", (req, res) => {
    res.send("apollo working")
})

router.get('/audio/generate/url', async (req, res) => {
    try {
        const data = await generateuploadURLToS3()
        res.send(data)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error
        })
    }
})

router.post('/audio/save', async (req, res) => {
    try {
        const data = await audio.create(req.body)
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get('/audio', async (req, res) => {
    try {
        const data = await audio.find({})
        res.send(data)
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

router.get('/audio/play/:fileName', async (req, res) => {
    try {
        res.sendFile(path.join(__dirname, `../Audio/${req.params.fileName}`))
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
})

module.exports = router