const express = require('express')
const router = express.Router()
const puppeteer = require('puppeteer');
const { addBase64DataToS3Bucket } = require('../Functions/S3')

router.post('/generate/image', async (req, res) => {
    try {
        // initialize the canvas data
        const templateData = JSON.parse(JSON.stringify(req.body.template))

        let imageUrl

        const template = {
            arrayOfElements: templateData[0].arrayOfElements,
            backgroundColor: templateData[0].backgroundColor,
            canvasWidth: req.body.canvasWidth,
            canvasHeight: req.body.canvasHeight,
            canvasMultiply: req.body.canvasMultiply
        }

        const saveTheBase64ToS3Bucket = async (data) => {
            const URL = await addBase64DataToS3Bucket(data)
            imageUrl = URL
            return URL
        }

        // set up the browser
        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox'],
            //executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
            executablePath: '/usr/bin/chromium-browser'
        });

        // open new page in browser
        const page = await browser.newPage();

        // To call the external node function from the browser
        await page.exposeFunction("saveTheBase64ToS3Bucket", saveTheBase64ToS3Bucket);

        await page.addScriptTag({
            path: './node_modules/webfontloader/webfontloader.js'
        });

        await page.addScriptTag({
            path: './node_modules/axios/dist/axios.js',
        });

        // browser code here
        await page.evaluate(async (template) => {
            // initialize the canvas
            const canvas = document.createElement('canvas')
            const ctx = canvas.getContext('2d')
            document.body.appendChild(canvas)

            const canvasGlobalOffScreen = document.createElement('canvas')
            canvasGlobalOffScreen.style.position = "absolute"
            canvasGlobalOffScreen.style.top = "-100000px"
            const ctxGlobalOffScreen = canvasGlobalOffScreen.getContext('2d')
            document.body.appendChild(canvasGlobalOffScreen)

            const canvasGlobalOffScreenBackgroundBlur = document.createElement('canvas')
            canvasGlobalOffScreen.style.position = "absolute"
            canvasGlobalOffScreen.style.top = "-100000px"
            const ctxGlobalOffScreenBackgroundBlur = canvasGlobalOffScreenBackgroundBlur.getContext('2d')
            document.body.appendChild(canvasGlobalOffScreenBackgroundBlur)

            // set height width & color to canvas
            ctx.save()
            canvas.width = template.canvasWidth * template.canvasMultiply
            canvas.height = template.canvasHeight * template.canvasMultiply
            ctx.fillStyle = template.backgroundColor;
            ctx.fillRect(0, 0, template.canvasWidth * template.canvasMultiply, template.canvasHeight * template.canvasMultiply);
            ctx.restore()

            // set height width & color to global offscreen canvas
            ctxGlobalOffScreen.save()
            canvasGlobalOffScreen.width = template.canvasWidth * template.canvasMultiply
            canvasGlobalOffScreen.height = template.canvasHeight * template.canvasMultiply
            ctxGlobalOffScreen.restore()

            // set height width & color to global background blur offscreen canvas
            ctxGlobalOffScreenBackgroundBlur.save()
            canvasGlobalOffScreenBackgroundBlur.width = template.canvasWidth * template.canvasMultiply
            canvasGlobalOffScreenBackgroundBlur.height = template.canvasHeight * template.canvasMultiply
            ctxGlobalOffScreenBackgroundBlur.restore()

            const addFontWeight = (data) => {
                ['100', '200', '300', '400', '500', '600', '700', '800', '900'].forEach(data1 => {
                    let text = document.createElement('div')
                    text.innerHTML = "hi"
                    text.style.fontFamily = data.fontFamily
                    text.style.fontWeight = data1
                    document.body.appendChild(text)
                })
            }

            const loadFonts = () => {
                let fonts = []
                template.arrayOfElements.forEach(data1 => {
                    if (data1.type === "text") {
                        fonts.push(data1.fontFamily)
                        addFontWeight(data1)
                    }
                    if (data1.type === "group") {
                        data1.data.forEach(data2 => {
                            if (data2.type === "text") {
                                fonts.push(data2.fontFamily)
                                addFontWeight(data2)
                            }
                        })
                    }
                })

                if (fonts.length <= 0) return
                fonts.forEach(data => {
                    if (data === 'poppins') {
                        data = "Poppins"
                    }
                    window.WebFont.load({
                        google: {
                            families: [data + ":100, 200, 300, 400, 500, 600, 700, 800, 900"]
                        },
                        timeout: 5000,
                    })
                })
            }

            //load the fonts
            loadFonts()

            const getSvgContent = async (src) => {
                try {
                    const response = await window.axios.get(src)
                    const text = response.data
                    return text
                } catch (error) {
                    console.log(error)
                }
            }

            const createImage = (data) => new Promise((resolve, reject) => {
                let image = document.createElement('img')
                image.src = data.src
                image.crossOrigin = "anonymous"
                image.id = data.elementId
                image.style.width = "2px";
                image.style.height = "2px";
                image.style.objectFit = "contain"
                image.addEventListener('load', () => resolve(image));
                image.addEventListener('error', (err) => reject(err));
                document.body.appendChild(image)
            })

            const createSvg = (data) => new Promise((resolve, reject) => {
                let image = document.createElement('img')
                image.src = data.src
                image.id = data.elementId
                image.crossOrigin = "anonymous"
                image.style.width = "2px";
                image.style.height = "2px";
                image.style.objectFit = "contain"
                image.addEventListener('load', () => resolve(image));
                image.addEventListener('error', (err) => reject(err));
                document.body.appendChild(image)
            })

            const createMask = (data) => new Promise((resolve, reject) => {
                let image = document.createElement('img')
                image.src = data.src
                image.id = data.elementId
                image.crossOrigin = "anonymous"
                image.style.width = "2px";
                image.style.height = "2px";
                image.style.objectFit = "contain"
                document.body.appendChild(image)

                //preview svg image
                let imagePreview = document.createElement('img')
                imagePreview.src = data.src1
                imagePreview.id = data.elementId2
                imagePreview.crossOrigin = "anonymous"
                imagePreview.style.width = "2px";
                imagePreview.style.height = "2px";
                imagePreview.style.objectFit = "contain"
                imagePreview.addEventListener('load', () => resolve(image));
                imagePreview.addEventListener('error', (err) => reject(err));
                document.body.appendChild(imagePreview)
            })

            const changeSvgColor = (data) => {
                const parser = new DOMParser();
                let doc = parser.parseFromString(data.contentDocument, 'text/html');
                let svg = doc.getElementsByTagName("svg")
                let path = doc.getElementsByTagName("path")
                let rect = doc.getElementsByTagName("rect")
                let circle = doc.getElementsByTagName("circle")
                let ellipse = doc.getElementsByTagName("ellipse")
                let line = doc.getElementsByTagName("line")
                let polyline = doc.getElementsByTagName("polyline")
                let polygon = doc.getElementsByTagName("polygon")
                let stop = doc.getElementsByTagName('stop')
                let text = doc.getElementsByTagName('text')
                let g = doc.getElementsByTagName('g')
                let style = doc.getElementsByTagName('style')

                let stopColors = [...stop]
                let allShapes = [...path, ...rect, ...circle, ...ellipse, ...line, ...polyline, ...polygon, ...g, ...text]

                addSvgColors(allShapes, stopColors, data, svg)

                let s = new XMLSerializer().serializeToString(svg[0])
                let encodedData = window.btoa(unescape(encodeURIComponent(s)))
                let str2 = "data:image/svg+xml;base64,"
                let url = str2.concat(encodedData);
                let img = document.getElementById(data.elementId)
                img.src = url
            }

            // convert rgb to hex code
            const rgb2hex = (rgb) => {
                rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
                return (rgb && rgb.length === 4) ? "#" +
                    ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                    ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                    ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
            }

            // add colors to svg element
            const addSvgColors = (allShapes, stopColors, data, svg) => {
                for (let i = 0; i < stopColors.length; i++) {
                    let object = stopColors[i].attributes
                    let n = object.length
                    for (let index = 0; index < n; index++) {
                        data.colorObject.forEach(data1 => {
                            if (data1.originalColor === object[index].value) {
                                object[index].value = data1.changeColor
                            }
                        })
                    }
                }

                for (let i = 0; i < stopColors.length; i++) {
                    let object = stopColors[i].style.stopColor
                    const colorData = rgb2hex(object)
                    data.colorObject.forEach(data1 => {
                        if (data1.originalColor === colorData) {
                            stopColors[i].style.stopColor = data1.changeColor
                        }
                    })
                }

                allShapes.forEach(data1 => {
                    data.colorObject.forEach(data2 => {
                        if (data2.originalColor === data1.getAttribute('fill')) {
                            data1.attributes.fill.value = data2.changeColor
                        }
                        if (data2.originalColor === data1.getAttribute('stroke')) {
                            data1.attributes.stroke.value = data2.changeColor
                        }
                        if (data1.style.fill !== "") {
                            const tempColor = rgb2hex(data1.style.fill)
                            if (data2.originalColor === tempColor) {
                                data1.style.fill = data2.changeColor
                            }
                        }

                        if (data1.style.stroke !== "") {
                            const tempColor = rgb2hex(data1.style.stroke)
                            if (data2.originalColor === tempColor) {
                                data1.style.stroke = data2.changeColor
                            }
                        }
                    })
                })
                data.contentDocument = new XMLSerializer().serializeToString(svg[0])
            }


            for await (let elements of template.arrayOfElements) {
                if (elements.type === "image") {
                    await createImage(elements)
                } else if (elements.type === "svg") {
                    await createSvg(elements)
                    elements.contentDocument = await getSvgContent(elements.src)
                    changeSvgColor(elements)
                } else if (elements.type === "mask") {
                    await createMask(elements)
                    elements.contentDocument = await getSvgContent(elements.src)
                    changeSvgColor(elements)
                } else if (elements.type === "group") {
                    for await (let groupElements of elements.data) {
                        if (groupElements.type === "image") {
                            await createImage(groupElements)
                        } else if (groupElements.type === "svg") {
                            await createSvg(groupElements)
                            groupElements.contentDocument = await getSvgContent(groupElements.src)
                            changeSvgColor(groupElements)
                        } else if (groupElements.type === "mask") {
                            await createMask(groupElements)
                            groupElements.contentDocument = await getSvgContent(groupElements.src)
                            changeSvgColor(groupElements)
                        }
                    }
                }
            }

            await new Promise(resolve => timeOut = setTimeout(resolveTheTimeOut = resolve, 2000));

            // rotate canvas element
            const rotateElement = (data, ctx) => {
                let x
                let y
                let width
                let height
                if (data.crop) {
                    x = data.cropX
                    y = data.cropY
                    width = data.cropWidth
                    height = data.cropHeight
                } else {
                    x = data.x
                    y = data.y
                    width = data.width
                    height = data.height
                }
                ctx.translate(x + (width / 2), y + (height / 2))
                ctx.rotate((Math.PI / 180) * data.rotate);
                ctx.translate(-x - (width / 2), -y - (height / 2));
            }

            // blur element
            const blurElement = (data, ctx) => {
                ctx.filter = `blur(${data.blur}px)`
            }

            // give element to shadow
            const shadowToElement = (data, ctx) => {
                ctx.shadowColor = data.shadowColor;
                ctx.shadowBlur = data.shadowBlur;
                ctx.shadowOffsetX = data.shadowOffsetX;
                ctx.shadowOffsetY = data.shadowOffsetY;
            }

            // skew canvas element
            const skewElement = (data, ctx) => {
                ctx.transform(1, data.skewY, data.skewX, 1, 0, 0)
            }

            // vertical flip
            const flipVertical = (x, y, height, ctx) => {
                ctx.translate(x, y);
                ctx.scale(1, -1);
                ctx.translate(-x, -(y + height));
            }

            // horizontal flip
            const flipHorizontal = (x, y, width, ctx) => {
                ctx.translate(x, y);
                ctx.scale(-1, 1);
                ctx.translate(-(x + width), -y);
            }

            const getLineHeight = (data) => {
                let lineHeight = parseFloat(data.lineHeight)
                let line = document.createElement('div')
                let body = document.body;
                line.style.position = 'absolute';
                line.style.whiteSpace = 'nowrap';
                line.style.transformOrigin = 'center'
                line.style.transform = `scale(${template.canvasMultiply}, ${template.canvasMultiply})`
                line.style.font = data.size + 'px ' + data.fontFamily;
                body.appendChild(line);
                line.innerHTML = 'm';
                if (isNaN(lineHeight)) {
                    line.style.lineHeight = line.clientHeight + "px"
                } else {
                    line.style.lineHeight = line.clientHeight + lineHeight + "px"
                }
                let height = line.offsetHeight;

                var span = document.createElement('span');
                span.style.display = 'inline-block';
                span.style.overflow = 'hidden';
                span.style.width = '1px';
                span.style.height = '1px';
                span.style.transformOrigin = 'center'
                line.style.transform = `scale(${template.canvasMultiply}, ${template.canvasMultiply})`

                line.appendChild(span);

                let baseline = span.offsetTop + span.offsetHeight;
                line.remove()

                return {
                    height: height,
                    baseline: baseline
                }
            }

            const letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

            const drawTextF = (text, x, y, w, h, hAlign, vAlign, lineheight, fn, data, context) => {
                if (data.stroke) {
                    context.lineWidth = data.strokeWidth;
                    context.strokeStyle = data.strokeColor
                    fn = "strokeText"
                }

                if (data.hollow) {
                    context.lineWidth = data.hollowWidth;
                    context.strokeStyle = data.hollowColor;
                    fn = "hollow"
                }

                let textMatrix = getLineHeight(data)
                let baseline = textMatrix.baseline
                lineheight = textMatrix.height
                data.actuallineHeight = lineheight
                data.baseline = baseline

                text = text.replace(/[\n]/g, " \n ");
                text = text.replace(/\r/g, "");
                var words = text.split(/[ ]+/);
                var sp = context.measureText(' ').width
                var lines = [];
                var actualline = 0;
                var actualsize = 0;
                var wo;
                lines[actualline] = {};
                lines[actualline].Words = [];
                let i = 0;

                while (i < words.length) {
                    var word = words[i];
                    if (word === "\n") {
                        lines[actualline].EndParagraph = true;
                        actualline++;
                        actualsize = 0;
                        lines[actualline] = {};
                        lines[actualline].Words = [];
                        i++;
                    } else {
                        wo = {};
                        wo.l = context.measureText(word).width;
                        if (actualsize === 0) {
                            while (wo.l > w) {
                                if (word.length === 1) {
                                    data.width = 6
                                    break;
                                }
                                word = word.slice(0, word.length - 1);
                                wo.l = context.measureText(word).width;
                            }

                            wo.word = word;
                            lines[actualline].Words.push(wo);
                            actualsize = wo.l;
                            if (word !== words[i]) {
                                words[i] = words[i].slice(word.length, words[i].length);
                            } else {
                                i++;
                            }
                        } else {
                            if (actualsize + sp + wo.l > w) {
                                lines[actualline].EndParagraph = false;
                                actualline++;
                                actualsize = 0;
                                lines[actualline] = {};
                                lines[actualline].Words = [];
                            } else {
                                wo.word = word;
                                lines[actualline].Words.push(wo);
                                actualsize += sp + wo.l;
                                i++;
                            }
                        }
                    }
                }

                lines[actualline].EndParagraph = true;
                let totalH = lineheight * lines.length
                data.height = totalH

                let yy;
                if (vAlign === "bottom") {
                    yy = y + h - totalH + lineheight;
                } else if (vAlign === "center") {
                    yy = y + h / 2 - totalH / 2 + lineheight;
                } else {
                    yy = y
                }

                let oldTextAlign = context.textAlign;
                context.textAlign = "left";

                for (var li in lines) {
                    var totallen = 0;
                    var xx, usp;
                    for (wo in lines[li].Words) totallen += lines[li].Words[wo].l;
                    if (hAlign === "center") {
                        usp = sp;
                        xx = x + w / 2 - (totallen + sp * (lines[li].Words.length - 1)) / 2;
                    } else if ((hAlign === "justify") && (!lines[li].EndParagraph)) {
                        xx = x;
                        usp = (w - totallen) / (lines[li].Words.length - 1);
                    } else if (hAlign === "right") {
                        xx = x + w - (totallen + sp * (lines[li].Words.length - 1));
                        usp = sp;
                    } else { // left
                        xx = x;
                        usp = sp;
                    }

                    if (data.list === "dot") {
                        let width = context.measureText("•").width
                        if (fn === "fillText") {
                            context.fillText("•", xx, yy + lineheight * li + baseline);
                        } else if (fn === "strokeText") {
                            context.fillText("•", xx, yy + lineheight * li + baseline);
                            context.strokeText("•", xx, yy + lineheight * li + baseline);
                        } else if (fn === "hollow") {
                            context.strokeText("•", xx, yy + lineheight * li + baseline);
                        }
                        xx += width + 2
                    } else if (data.list === "number") {
                        let number = parseInt(li) + 1
                        let width = context.measureText(number + ".").width
                        if (fn === "fillText") {
                            context.fillText(number + ".", xx, yy + lineheight * li + baseline);
                        } else if (fn === "strokeText") {
                            context.fillText(number + ".", xx, yy + lineheight * li + baseline);
                            context.strokeText(number + ".", xx, yy + lineheight * li + baseline);
                        } else if (fn === "hollow") {
                            context.strokeText(number + ".", xx, yy + lineheight * li + baseline);
                        }
                        xx += width + 2
                    } else if (data.list === "abc") {
                        let width = context.measureText(letters[li] + ".").width
                        if (fn === "fillText") {
                            context.fillText(letters[li] + ".", xx, yy + lineheight * li + baseline);
                        } else if (fn === "strokeText") {
                            context.fillText(letters[li] + ".", xx, yy + lineheight * li + baseline);
                            context.strokeText(letters[li] + ".", xx, yy + lineheight * li + baseline);
                        } else if (fn === "hollow") {
                            context.strokeText(letters[li] + ".", xx, yy + lineheight * li + baseline);
                        }
                        xx += width + 2
                    }

                    for (wo in lines[li].Words) {
                        if (fn === "fillText") {
                            context.fillText(lines[li].Words[wo].word, xx, yy + lineheight * li + baseline);
                        } else if (fn === "strokeText") {
                            context.fillText(lines[li].Words[wo].word, xx, yy + lineheight * li + baseline);
                            context.strokeText(lines[li].Words[wo].word, xx, yy + lineheight * li + baseline);
                        } else if (fn === "hollow") {
                            context.strokeText(lines[li].Words[wo].word, xx, yy + lineheight * li + baseline);
                        }
                        xx += lines[li].Words[wo].l + usp;
                    }
                }

                context.textAlign = oldTextAlign;
            }

            // draw offscreen composition method
            const drawImageOffscreen = (data, id) => {
                if (data.id === id) {
                    ctxGlobalOffScreen.globalCompositeOperation = data.blendMode
                }

                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId);

                ctxGlobalOffScreen.save()
                ctxGlobalOffScreen.scale(template.canvasMultiply, template.canvasMultiply)

                shadowToElement(data, ctxGlobalOffScreen)
                skewElement(data, ctxGlobalOffScreen)

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreen)
                }

                if (data.flipHorizontal) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreen)
                }

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const ctx1 = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = template.canvasHeight * template.canvasMultiply

                    ctx1.save()
                    ctx1.scale(template.canvasMultiply, template.canvasMultiply)
                    ctx1.drawImage(img, x, y, width, height)
                    ctx1.globalCompositeOperation = 'destination-in'
                    ctx1.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctx1.restore()

                    ctxGlobalOffScreen.globalAlpha = data.opacity
                    ctxGlobalOffScreen.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctxGlobalOffScreen.globalAlpha = 1.0;
                } else {
                    ctxGlobalOffScreen.globalAlpha = data.opacity
                    ctxGlobalOffScreen.drawImage(img, x, y, width, height);
                    ctxGlobalOffScreen.globalAlpha = 1.0;
                }
                ctxGlobalOffScreen.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreen.restore()
            }

            const drawTextOffscreen = (data, id) => {
                if (data.id === id) {
                    ctxGlobalOffScreen.globalCompositeOperation = data.blendMode
                }

                ctxGlobalOffScreen.save()
                ctxGlobalOffScreen.scale(template.canvasMultiply, template.canvasMultiply)
                ctxGlobalOffScreen.globalAlpha = data.opacity
                ctxGlobalOffScreen.font = data.italic + " " + data.bold + " " + data.size + "px " + data.fontFamily
                ctxGlobalOffScreen.fillStyle = data.color;

                blurElement(data, ctxGlobalOffScreen)
                shadowToElement(data, ctxGlobalOffScreen)
                skewElement(data, ctxGlobalOffScreen)
                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreen)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreen)
                }

                canvasGlobalOffScreen.style.letterSpacing = data.letterSpacing + 'px';
                drawTextF(data.content, data.x, data.y, data.width, data.height, data.align, "top", (data.size - (data.size / 8)), "fillText", data, ctxGlobalOffScreen)
                ctxGlobalOffScreen.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreen.restore()
            }

            const drawSvgOffscreen = (data, id) => {
                if (data.id === id) {
                    ctxGlobalOffScreen.globalCompositeOperation = data.blendMode
                }

                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId);

                ctxGlobalOffScreen.save()
                ctxGlobalOffScreen.scale(template.canvasMultiply, template.canvasMultiply)
                blurElement(data, ctxGlobalOffScreen)
                shadowToElement(data, ctxGlobalOffScreen)
                skewElement(data, ctxGlobalOffScreen)

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreen)
                }

                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreen)
                }

                ctxGlobalOffScreen.globalAlpha = data.opacity

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const ctx1 = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = teamplte.canvasHeight * template.canvasMultiply

                    ctx1.save()
                    ctx1.scale(template.canvasMultiply, template.canvasMultiply)
                    ctx1.drawImage(img, x, y, width, height)
                    ctx1.globalCompositeOperation = 'destination-in'
                    ctx1.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctx1.restore()

                    ctxGlobalOffScreen.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                } else {
                    ctxGlobalOffScreen.drawImage(img, x, y, width, height);
                }

                ctxGlobalOffScreen.globalAlpha = 1.0;
                ctxGlobalOffScreen.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreen.restore()
            }

            const drawMaskOffscreen = (data, id) => {
                if (data.id === id) {
                    ctxGlobalOffScreen.globalCompositeOperation = data.blendMode
                }

                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId)
                let img1 = document.getElementById(data.elementId2)

                const canvasMask = document.createElement('canvas')
                const ctxMask = canvasMask.getContext('2d')
                document.body.appendChild(canvasMask)

                canvasMask.width = template.canvasWidth * template.canvasMultiply
                canvasMask.height = template.canvasHeight * template.canvasMultiply

                ctxMask.save()
                ctxMask.clearRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.fillRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.scale(template.canvasMultiply, template.canvasMultiply)
                ctxMask.drawImage(img1, data.x1, data.y1, data.width1, data.height1);
                ctxMask.globalCompositeOperation = 'destination-in';
                ctxMask.drawImage(img, x, y, width, height);
                ctxMask.restore()

                ctxGlobalOffScreen.save()
                ctxGlobalOffScreen.scale(template.canvasMultiply, template.canvasMultiply)
                blurElement(data, ctxGlobalOffScreen)
                shadowToElement(data, ctxGlobalOffScreen)
                skewElement(data, ctxGlobalOffScreen)
                ctxGlobalOffScreen.drawImage(canvasMask, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height);
                ctxGlobalOffScreen.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreen.restore()
            }

            const drawGroupOffscreen = (data, id, type) => {
                data.data.forEach(element => {
                    if (element.type === "image") {
                        drawImageOffscreen(element, id, type)
                    } if (element.type === "svg") {
                        drawSvgOffscreen(element, id)
                    } if (element.type === "text") {
                        drawTextOffscreen(element, id)
                    } if (element.type === "mask") {
                        drawMaskOffscreen(element, id)
                    }
                })
            }

            // global composition methods
            const drawOffScreenCanvasGlobalComposition = (element, type) => {

                let stop = true

                ctxGlobalOffScreen.clearRect(0, 0, template.canvasWidth * template.canvasMultiply, template.canvasHeight * template.canvasMultiply);
                canvasGlobalOffScreen.width = template.canvasWidth * template.canvasMultiply
                canvasGlobalOffScreen.height = template.canvasHeight * template.canvasMultiply
                ctxGlobalOffScreen.fillStyle = template.backgroundColor;
                ctxGlobalOffScreen.fillRect(0, 0, template.canvasWidth * template.canvasMultiply, template.canvasHeight * template.canvasMultiply);

                //start drawing on the canvas
                template.arrayOfElements.forEach(data => {
                    if (data.display === false) return
                    if (stop === false) return
                    if (data.type === "image") {
                        drawImageOffscreen(data, element.id, type)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "text") {
                        drawTextOffscreen(data, element.id)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "svg") {
                        drawSvgOffscreen(data, element.id)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "mask") {
                        drawMaskOffscreen(data, element.id)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "group") {
                        drawGroupOffscreen(data, element.id, type)
                        if (data.id === element.id) return stop = false
                    }
                })
            }

            const applyBlendMode = (data) => {
                drawOffScreenCanvasGlobalComposition(data)
                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                rotateElement(data, ctx)
                ctx.globalAlpha = data.opacity
                ctx.drawImage(canvasGlobalOffScreen, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height);
                ctx.restore()
                ctx.globalAlpha = 1
            }


            const drawImageBackgroundBlur = (data, activeElement) => {
                if (data.id === activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.save()
                    ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = data.opacity
                    let img = document.getElementById(data.elementId);
                    ctxGlobalOffScreenBackgroundBlur.drawImage(img, data.x, data.y, data.width, data.height)
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                    ctxGlobalOffScreenBackgroundBlur.restore()
                    ctxGlobalOffScreenBackgroundBlur.globalCompositeOperation = "destination-in"
                }

                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId);

                ctxGlobalOffScreenBackgroundBlur.save()
                ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)
                shadowToElement(data, ctxGlobalOffScreenBackgroundBlur)
                skewElement(data, ctxGlobalOffScreenBackgroundBlur)
                if (data.id !== activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.filter = `blur(${activeElement.backgroundBlur}px)`
                }

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreenBackgroundBlur)
                }

                if (data.flipHorizontal) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreenBackgroundBlur)
                }

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const ctx1 = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = template.canvasHeight * template.canvasMultiply

                    ctx1.save()
                    ctx1.scale(template.canvasMultiply, template.canvasMultiply)
                    ctx1.drawImage(img, x, y, width, height)
                    ctx1.globalCompositeOperation = 'destination-in'
                    ctx1.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctx1.restore()

                    ctxGlobalOffScreenBackgroundBlur.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                } else {
                    ctxGlobalOffScreenBackgroundBlur.drawImage(img, x, y, width, height);
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                }

                ctxGlobalOffScreenBackgroundBlur.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreenBackgroundBlur.restore()
            }

            const drawTextBackgroundBlur = (data, activeElement) => {
                if (data.id === activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.globalCompositeOperation = "destination-in"
                }

                ctxGlobalOffScreenBackgroundBlur.save()
                ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)
                canvasGlobalOffScreenBackgroundBlur.style.letterSpacing = (data.letterSpacing) + 'px';
                ctxGlobalOffScreenBackgroundBlur.globalAlpha = data.opacity
                ctxGlobalOffScreenBackgroundBlur.font = data.italic + " " + data.bold + " " + data.size + "px " + data.fontFamily
                ctxGlobalOffScreenBackgroundBlur.fillStyle = data.color;

                if (data.id !== activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.filter = `blur(${activeElement.backgroundBlur}px)`
                }

                shadowToElement(data, ctxGlobalOffScreenBackgroundBlur)
                skewElement(data, ctxGlobalOffScreenBackgroundBlur)

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreenBackgroundBlur)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreenBackgroundBlur)
                }

                drawTextF(data.content, data.x, data.y, data.width, data.height, data.align, "top", (data.size - (data.size / 8)), "fillText", data, ctxGlobalOffScreenBackgroundBlur)
                ctxGlobalOffScreenBackgroundBlur.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreenBackgroundBlur.restore()
            }

            const drawSvgBackgroundBlur = (data, activeElement) => {
                if (data.id === activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.save()
                    ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = data.opacity
                    let img = document.getElementById(data.elementId)
                    ctxGlobalOffScreenBackgroundBlur.drawImage(img, data.x, data.y, data.width, data.height)
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                    ctxGlobalOffScreenBackgroundBlur.restore()
                    ctxGlobalOffScreenBackgroundBlur.globalCompositeOperation = "destination-in"
                }

                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height

                let img = document.getElementById(data.elementId)

                ctxGlobalOffScreenBackgroundBlur.save()
                ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)
                shadowToElement(data, ctxGlobalOffScreenBackgroundBlur)
                skewElement(data, ctxGlobalOffScreenBackgroundBlur)

                if (data.id !== activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.filter = `blur(${activeElement.backgroundBlur}px)`
                }

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctxGlobalOffScreenBackgroundBlur)
                }

                if (data.flipHorizontal) {
                    flipHorizontal(data.x, data.y, data.width, ctxGlobalOffScreenBackgroundBlur)
                }

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const ctx1 = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = template.canvasHeight * template.canvasMultiply

                    ctx1.save()
                    ctx1.scale(template.canvasMultiply, template.canvasMultiply)
                    ctx1.drawImage(img, x, y, width, height)
                    ctx1.globalCompositeOperation = 'destination-in'
                    ctx1.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    ctx1.restore()

                    ctxGlobalOffScreenBackgroundBlur.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                } else {
                    ctxGlobalOffScreenBackgroundBlur.drawImage(img, x, y, width, height);
                }
                ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                ctxGlobalOffScreenBackgroundBlur.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreenBackgroundBlur.globalCompositeOperation = "source-in"
                ctxGlobalOffScreenBackgroundBlur.restore()
            }

            const drawMaskBackgroundBlur = (data, activeElement) => {
                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId)
                let img1 = document.getElementById(data.elementId2)

                const canvasMask = document.createElement('canvas')
                const ctxMask = canvasMask.getContext('2d')

                canvasMask.width = template.canvasWidth * template.canvasMultiply
                canvasMask.height = template.canvasHeight * template.canvasMultiply

                ctxMask.save()
                ctxMask.clearRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.fillRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.scale(template.canvasMultiply, template.canvasMultiply)
                ctxMask.drawImage(img1, data.x1, data.y1, data.width1, data.height1);
                ctxMask.globalCompositeOperation = 'destination-in';
                ctxMask.drawImage(img, x, y, width, height);
                ctxMask.restore()

                ctxGlobalOffScreenBackgroundBlur.save()
                ctxGlobalOffScreenBackgroundBlur.scale(template.canvasMultiply, template.canvasMultiply)

                if (data.id === activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.save()
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = data.opacity
                    ctxGlobalOffScreenBackgroundBlur.drawImage(canvasMask, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height)
                    ctxGlobalOffScreenBackgroundBlur.globalAlpha = 1.0;
                    ctxGlobalOffScreenBackgroundBlur.restore()
                    ctxGlobalOffScreenBackgroundBlur.globalCompositeOperation = "destination-in"
                }

                if (data.id !== activeElement.id) {
                    ctxGlobalOffScreenBackgroundBlur.filter = `blur(${activeElement.backgroundBlur}px)`
                }

                shadowToElement(data, ctxGlobalOffScreenBackgroundBlur)
                skewElement(data, ctxGlobalOffScreenBackgroundBlur)
                ctxGlobalOffScreenBackgroundBlur.drawImage(canvasMask, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height);
                ctxGlobalOffScreenBackgroundBlur.transform(1, 0, 0, 1, 0, 0)
                ctxGlobalOffScreenBackgroundBlur.restore()
            }

            const drawGroupBackgroundBlur = (data, element) => {
                let stop = true
                data.data.forEach(data => {
                    if (stop === false) return
                    if (data.type === "image") {
                        drawImageBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "text") {
                        drawTextBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "svg") {
                        drawSvgBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "mask") {
                        drawMaskBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    }
                })
            }


            const drawOffScreenCanvasGlobalCompositionBackgroundBlur = (element) => {

                let stop = true

                ctxGlobalOffScreenBackgroundBlur.clearRect(0, 0, template.canvasWidth * template.canvasMultiply, template.canvasHeight * template.canvasMultiply);
                ctxGlobalOffScreenBackgroundBlur.width = template.canvasWidth * template.canvasMultiply
                ctxGlobalOffScreenBackgroundBlur.height = template.canvasHeight * template.canvasMultiply
                ctxGlobalOffScreenBackgroundBlur.fillStyle = template.backgroundColor;
                ctxGlobalOffScreenBackgroundBlur.fillRect(0, 0, template.canvasWidth * template.canvasMultiply, template.canvasHeight * template.canvasMultiply);

                //start drawing on the canvas
                template.arrayOfElements.forEach(data => {
                    if (data.display === false) return
                    if (stop === false) return
                    if (data.type === "image") {
                        drawImageBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "text") {
                        drawTextBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "svg") {
                        drawSvgBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "mask") {
                        drawMaskBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    } else if (data.type === "group") {
                        drawGroupBackgroundBlur(data, element)
                        if (data.id === element.id) return stop = false
                    }
                })
            }


            const applyBlendModeBackgroundBlur = (data) => {
                drawOffScreenCanvasGlobalCompositionBackgroundBlur(data)
                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                rotateElement(data, ctx)
                ctx.filter = `blur(${data.blur}px)`
                ctx.drawImage(canvasGlobalOffScreenBackgroundBlur, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height);
                ctx.restore()
            }

            // draw image in canvas
            const drawImage = (data) => {
                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId);

                if (data.blendMode !== undefined && data.blendMode !== "normal") return applyBlendMode(data)
                if (data.backgroundBlur !== 0) return applyBlendModeBackgroundBlur(data)

                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                rotateElement(data, ctx)
                shadowToElement(data, ctx)
                skewElement(data, ctx)
                ctx.filter = `brightness(${data.brightness}%) contrast(${data.contrast}%) grayscale(${data.grayscale}%) saturate(${data.saturate}%) sepia(${data.sepia}%) invert(${data.invert}%) blur(${data.blur}px) hue-rotate(${data.hueRotate}deg)`
                ctx.globalAlpha = data.opacity
                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctx)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctx)
                }

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const offctx = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = template.canvasHeight * template.canvasMultiply

                    offctx.save()
                    offctx.scale(template.canvasMultiply, template.canvasMultiply)
                    offctx.drawImage(img, x, y, width, height)
                    offctx.globalCompositeOperation = 'destination-in'
                    offctx.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                    offctx.restore()

                    ctx.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                } else {
                    ctx.drawImage(img, x, y, width, height);
                }


                ctx.transform(1, 0, 0, 1, 0, 0)
                ctx.globalAlpha = 1.0;
                ctx.restore()

                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                const temperature = parseInt(data.temperature)
                if (temperature !== 0) {
                    let imageData

                    let x
                    let y
                    let width
                    let height
                    if (data.crop) {
                        x = data.cropX
                        y = data.cropY
                        width = data.cropWidth
                        height = data.cropHeight
                    } else {
                        x = data.x
                        y = data.y
                        width = data.width
                        height = data.height
                    }

                    imageData = ctx.getImageData(x * template.canvasMultiply, y * template.canvasMultiply, width * template.canvasMultiply, height * template.canvasMultiply);

                    for (let i = 0; i < imageData.data.length; i += 4) {
                        let red = imageData.data[i]
                        let green = imageData.data[i + 1]
                        let blue = imageData.data[i + 2]

                        imageData.data[i] = red + temperature;
                        imageData.data[i + 1] = green;
                        imageData.data[i + 2] = blue - temperature;
                    }


                    ctx.putImageData(imageData, x * template.canvasMultiply, y * template.canvasMultiply);
                }

                const tint = parseInt(data.tint)
                if (tint !== 0) {
                    let imageData

                    let x
                    let y
                    let width
                    let height
                    if (data.crop) {
                        x = data.cropX
                        y = data.cropY
                        width = data.cropWidth
                        height = data.cropHeight
                    } else {
                        x = data.x
                        y = data.y
                        width = data.width
                        height = data.height
                    }

                    imageData = ctx.getImageData(x * template.canvasMultiply, y * template.canvasMultiply, width * template.canvasMultiply, height * template.canvasMultiply);

                    for (let i = 0; i < imageData.data.length; i += 4) {
                        let red = imageData.data[i]
                        let green = imageData.data[i + 1]
                        let blue = imageData.data[i + 2]

                        imageData.data[i] = red;
                        imageData.data[i + 1] = green + tint;
                        imageData.data[i + 2] = blue;
                    }

                    ctx.putImageData(imageData, x * template.canvasMultiply, y * template.canvasMultiply);
                }

                ctx.restore()
            }

            // draw text on canvas 
            const drawText = (data) => {
                canvas.style.letterSpacing = data.letterSpacing + 'px';

                if (data.blendMode !== undefined && data.blendMode !== "normal") return applyBlendMode(data)
                if (data.backgroundBlur !== 0) return applyBlendModeBackgroundBlur(data)

                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                ctx.globalAlpha = data.opacity

                if (data.canvasBold) {
                    ctx.font = data.italic + " bold " + data.size + "px " + data.fontFamily
                } else {
                    ctx.font = data.italic + " " + data.bold + " " + data.size + "px " + data.fontFamily
                }

                ctx.fillStyle = data.color;

                rotateElement(data, ctx)
                blurElement(data, ctx)
                shadowToElement(data, ctx)
                skewElement(data, ctx)

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctx)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctx)
                }

                drawTextF(data.content, data.x, data.y, data.width, data.height, data.align, "top", (data.size - (data.size / 8)), "fillText", data, ctx)

                ctx.transform(1, 0, 0, 1, 0, 0)
                ctx.globalAlpha = 1.0;
                ctx.restore()
            }

            // draw svg on canvas 
            const drawSvg = async (data) => {
                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId);

                if (data.blendMode !== undefined && data.blendMode !== "normal") return applyBlendMode(data)
                if (data.backgroundBlur !== 0) return applyBlendModeBackgroundBlur(data)

                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)

                rotateElement(data, ctx)
                blurElement(data, ctx)
                shadowToElement(data, ctx)
                skewElement(data, ctx)
                ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctx)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctx)
                }

                ctx.globalAlpha = data.opacity

                if (data.crop) {
                    const offscreen = new OffscreenCanvas(template.canvasWidth, template.canvasHeight);
                    const ctx1 = offscreen.getContext('2d');
                    offscreen.width = template.canvasWidth * template.canvasMultiply
                    offscreen.height = template.canvasHeight * template.canvasMultiply

                    ctx1.drawImage(img, x, y, width, height)
                    ctx1.globalCompositeOperation = 'destination-in'
                    ctx1.fillRect(data.cropX, data.cropY, data.cropWidth, data.cropHeight);

                    ctx.drawImage(offscreen, data.cropX * template.canvasMultiply, data.cropY * template.canvasMultiply, data.cropWidth * template.canvasMultiply, data.cropHeight * template.canvasMultiply, data.cropX, data.cropY, data.cropWidth, data.cropHeight);
                } else {
                    ctx.drawImage(img, x, y, width, height);
                }

                ctx.transform(1, 0, 0, 1, 0, 0)
                ctx.globalAlpha = 1.0;
                ctx.restore()
            }

            // draw mask on canvas 
            const drawMask = (data) => {
                let x = data.x
                let y = data.y
                let width = data.width
                let height = data.height
                let img = document.getElementById(data.elementId)
                let img1 = document.getElementById(data.elementId2)


                if (data.blendMode !== undefined && data.blendMode !== "normal") return applyBlendMode(data)
                if (data.backgroundBlur !== 0) return applyBlendModeBackgroundBlur(data)

                const canvasMask = document.createElement('canvas')
                const ctxMask = canvasMask.getContext('2d')
                document.body.appendChild(canvasMask)

                canvasMask.width = template.canvasWidth * template.canvasMultiply
                canvasMask.height = template.canvasHeight * template.canvasMultiply

                ctxMask.save()
                ctxMask.clearRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.fillRect(0, 0, canvasMask.width, canvasMask.height);
                ctxMask.scale(template.canvasMultiply, template.canvasMultiply)
                ctxMask.drawImage(img1, data.x1, data.y1, data.width1, data.height1);
                ctxMask.globalCompositeOperation = 'destination-in';
                ctxMask.drawImage(img, x, y, width, height);
                ctxMask.restore()

                ctx.save()
                ctx.scale(template.canvasMultiply, template.canvasMultiply)
                rotateElement(data, ctx)
                shadowToElement(data, ctx)
                blurElement(data, ctx)
                skewElement(data, ctx)
                ctx.globalAlpha = data.opacity

                if (data.verticalFlip) {
                    flipVertical(data.x, data.y, data.height, ctx)
                }
                if (data.horizontalFlip) {
                    flipHorizontal(data.x, data.y, data.width, ctx)
                }

                ctx.drawImage(canvasMask, data.x * template.canvasMultiply, data.y * template.canvasMultiply, data.width * template.canvasMultiply, data.height * template.canvasMultiply, data.x, data.y, data.width, data.height);

                ctx.globalAlpha = 1
                ctx.transform(1, 0, 0, 1, 0, 0)
                ctx.restore()
            }

            const drawGroup = (data) => {
                data.data.forEach(element => {
                    if (element.type === "image") {
                        drawImage(element)
                    } else if (element.type === "text") {
                        drawText(element)
                    } else if (element.type === "svg") {
                        drawSvg(element)
                    } else if (element.type === "mask") {
                        drawMask(element)
                    }
                })
            }

            // start drawing canvas
            const draw = () => {
                template.arrayOfElements.forEach(element => {
                    if (element.display === false) return
                    if (element.type === "image") {
                        drawImage(element)
                    } else if (element.type === "text") {
                        drawText(element)
                    } else if (element.type === "svg") {
                        drawSvg(element)
                    } else if (element.type === "mask") {
                        drawMask(element)
                    } else if (element.type === "group") {
                        drawGroup(element)
                    }
                })
            }

            draw()

            let base64Image = canvas.toDataURL('image/png', 1)
            await saveTheBase64ToS3Bucket(base64Image)
        }, template)

        // close the browser
        await browser.close()

        res.send(imageUrl)
    } catch (error) {
        console.log(error)
        res.status(500).json({
            error: error
        })
    }
})

module.exports = router