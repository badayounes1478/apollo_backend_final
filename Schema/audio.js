const mongoose = require('mongoose')
const schema = mongoose.Schema

const Audio = new schema({
    user_id: String,
    tags: [String],
    category: String,
    url: String,
    file_name: String
}, { timestamps: true })

module.exports = mongoose.model('Audio', Audio)
